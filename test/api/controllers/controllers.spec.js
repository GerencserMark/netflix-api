const { expect } = require('chai');
var chai = require('chai');
var should = chai.should();
var request = require('supertest');
const videos = require('../../../api/controllers/videos');
var server = require('../../../app');
var sinon = require('sinon');
const repository = require('../../../api/helpers/repository');

let sessionID;

describe('controllers', function () {
    describe('videos', function () {
        describe('POST /videos', function () {

            it('should push new video to db', async function () {
                const data =
                {
                    type: "Movies",
                    category: {
                        name: "drama"
                    },
                    name: "Titanic"
                };
                const res = await request(server).post('/videos').set('X-Admin-API-key', '123456').send(data);
                expect(res.body.name).to.equal(data.name);
            })
        })
        describe('GET /videos', function () {
            it('should return searched video', async function () {
                const spy = sinon.spy(videos, "getVideoByTitle");
                const res = await request(server).get('/videos?name=Titanic').set('X-Session-ID', '1234567');
                expect(res.body[0].name).to.equal("Titanic");
                sinon.assert.calledOnce(spy);
                videos.getVideoByTitle.restore();
            })
            it('should return empty array if video not in DB', async function () {
                const res = await request(server).get('/videos?name=Batman').set('X-Session-ID', '1234567');
                if (res.body.length === 0) {
                    expect.fail("Video not found!");
                }

            })
        })
        describe('stub videos controller, getVideoByTitle', function () {
            before(function() {
                const data = {
                    username: "TestUser2",
                    password: "TestPassword2"
                }
                const res = await request(server).post('/login').send(data);
                sessionID = res.body.sessionID;
            });
            after(function () {
                sessionID = null;
            });
            it('should return searched video', async function () {
                const data =
                {
                    type: "Movies",
                    category: {
                        name: "drama"
                    },
                    name: "Titanic"
                };
                var stub = sinon.stub(videos, "getVideoByTitle");
                stub.withArgs('Titanic').returns(
                    {
                    type: "Movies",
                    category: {
                        name: "drama"
                    },
                    name: "Titanic2"
                    });

                const res = await request(server).get('/videos?name=Titanic').set('X-Session-ID', sessionID);
                expect(res.body[0].name).to.be.equal("Titanic2");
                repository.find.restore();
            })
        })
    })
    describe('videos/{videoId}', function () {
        describe('GET /videos/{videoId}', function () {
            it('should return a video with {videoId}', async function () {
                const res = await request(server).get('/videos/1').set('X-Admin-API-key', '123456');
                expect(res.body[0].name).to.equal("Titanic");
            })
        })
        describe('PUT /videos/{videoId}', function () {
            it('should update the video with {videoId}', async function () {
                const data =
                {
                    id: 1,
                    type: "Movies",
                    category: {
                        name: "drama, romantic"
                    },
                    name: "Titanic"
                };
                const data2 =
                {
                    id: 1,
                    type: "Movies",
                    category: {
                        name: "drama, romantic"
                    },
                    name: "Titanic2"
                };
                const res = await request(server).put('/videos/1').set('X-Admin-API-key', '123456').send(data);
                expect(res.body[0].category.name).to.eql(data.category.name);
            })
        })
        describe('DELETE /videos/{videoId}', function () {
            it('should delete the video with {videoId}', async function () {
                const data = [];
                const res = await request(server).del('/videos/1').set('X-Admin-API-key', '123456');
                expect(res.body).to.eql(data);
            })
        })
    })
    describe('queue', function () {
        describe('POST /queue', function () {
            it('should update user queue', async function () {
                const order = {
                    videoId: 2,
                    name: "Batman"
                }
                const res = await request(server).post('/queue').set('X-Session-ID', '1234567').send(order);
                expect(res.body.name).to.equal(order.name);
            })
        })
        describe('GET /queue', function () {
            it('should return user queue', async function () {
                const dummyQueue = {
                    videoId: 2,
                    name: "Batman"
                };
                const res = await request(server).get('/queue').set('X-Session-ID', '1234567');
                expect(res.body[0].name).to.equal(dummyQueue.name);
            })
        })

    })
    describe('register', function () {
        describe('POST /register', function () {
            it('should update usersDb', async function () {
                const data = {
                    username: "TestUser",
                    password: "TestPassword"
                };
                const res = await request(server).post('/register').send(data);
                expect(res.body.username).to.equal(data.username);
            })
            it('should have password and username property', async function () {
                const data = {
                    username: "TestUser2",
                    password: "TestPassword2"
                };
                const res = await request(server).post('/register').send(data);
                expect(res.body).to.have.property('password');
                expect(res.body).to.have.property('username');
            })
        })
    })
    describe('login', function () {
        describe('POST /login', function () {
            it('login user', async function () {
                const data = {
                    username: "TestUser",
                    password: "TestPassword"
                };
                const res = await request(server).post('/login').send(data);
                expect(res.body.user[0].password).to.equal(data.password);
                expect(res.body.user[0].username).to.equal(data.password);
            })
            it('negative test case for login', async function () {
                const data = {
                    username: "TestUser",
                    password: "TestPassword3"
                };
                const res = await request(server).post('/login').send(data);
                expect.fail("Login failed!");
            })
        })
    })
    describe('logout', function () {
        describe('POST /logout', function () {
            it('should logout user', async function () {
                const data = {
                    username: undefined,
                    password: undefined
                };
                const res = await request(server).post('/logout').set('X-Session-ID', '1234567').send(data);
                expect(res.body.username).to.equal(data.username)
            })
        })
    })
})