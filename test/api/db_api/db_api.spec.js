const { assert } = require('chai');
const nock = require('nock');
const request = require('supertest');
const server = require('../../../app');

const dbApi = nock('http://localhost:3001/api/v1');

describe('dbApi', function() {
    it('update video by id', async function() {
        const expectedVideo = "Titanic";

        dbApi.put('/video/a1234')
            .reply(200, {
                name: expectedVideo,
                type: "Movie",
                category: {
                    name: "drama, romantic"
                }
            })
        const result = await request(server).put('/videos/a1234');
        assert.equal(result.body.name, expectedVideo);
    })
    it('get video by id', async function() {
        const expectedVideoName = "Titanic";

        dbApi.get('/video/a123')
            .reply(200, {
                name: expectedVideoName,
                type: 'Movie',
                category: {
                    name: "drama"
                }
            })
        const result = await request(server).get('/videos/a123');
        assert.equal(result.body.name, expectedVideoName);
    })
})