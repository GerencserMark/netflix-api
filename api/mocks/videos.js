'use strict';

const data = require('./videos.json');

module.exports = {
    getVideoByTitle: getVideoByTitle
};

function getVideoByTitle(req, res) {

    const title = req.swagger.params.name.value || 'Titanic';

    const filterItems = (arr, query) => {
        return arr.filter(el => el.name.indexOf(query) !== -1)
    }
      res.json(filterItems(data, title))
  };