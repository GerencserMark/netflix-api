'use strict';

const repository = require('../helpers/repository');
const uuid = require('uuid');
const rp = require('request-promise');

const KONG_URL = process.env.KONG_URL || 'http://kong:8001';

module.exports = {
    createUser: createUser,
    loginUser: loginUser,
    logoutUser: logoutUser
};

async function createUser(req, res) {
    const user = req.body;
    await rp({
        method: 'POST',
        uri: `${KONG_URL}/consumers`,
        body: 
        {
            "username": user.username,
            "tags":["user"]
        },
        json: true
    });
    await rp({
        method: 'POST',
        uri: `${KONG_URL}/consumers/${user.username}/acls`,
        body: {"group":"user"},
        json: true
    });
    await repository.insert('user', user);
    res.json(user);
}

async function loginUser(req, res) {
    const username = req.body.username;
    const password = req.body.password;
    const apiKey = uuid.v4();
    const user = await repository.find('user', (`"username":"${username}"`));
    if (typeof user[0] === 'undefined') {
        res.json("Username and password does not match!");
    } else if (user[0].username == username && user[0].password == password) {
        await rp({
            method: 'POST',
            uri: `${KONG_URL}/consumers/${username}/key-auth`,
            body: 
            {
                "key": `${apiKey}`
            },
            json: true
        });
        res.json("Login Successfull!");
    } else {
        res.json("Invalid credentials!")
    }
}

async function logoutUser(req, res) {
    const XConsumerUsername = await req.get("X-Consumer-Username");
    const result = await rp({
        method: 'GET',
        uri: `${KONG_URL}/consumers/${XConsumerUsername}/key-auth`,
        json: true
    });
    await rp({
        method: 'DELETE',
        uri: `${KONG_URL}/consumers/${XConsumerUsername}/key-auth/${result.data[0].id}`,
        json: true
    });
    res.json("Logout successfull!");
}