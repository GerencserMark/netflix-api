'use strict';

const repository = require('../helpers/repository');

module.exports = {
    getQueue: getQueue,
    placeOrder: placeOrder
};

async function getQueue(req, res) {
    const XConsumerUsername = await req.get("X-Consumer-Username");
    const queue = await repository.find('queue', (`"username":"${XConsumerUsername}"`));
    res.json(queue);
}

async function placeOrder(req, res) {
    const XConsumerUsername = await req.get("X-Consumer-Username");
    const video = {
        name: req.body.name,
        username: XConsumerUsername
    };
    await repository.insert('queue', video);
    res.json(video);
}