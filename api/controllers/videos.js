'use strict';

const repository = require('../helpers/repository');

module.exports = {
    addVideo: addVideo,
    getVideoByTitle: getVideoByTitle,
    getVideoById: getVideoById,
    updateVideo: updateVideo,
    deleteVideo: deleteVideo
};

function addVideo(req, res) {
    const video = req.body;
    repository.insert('video', video);
    res.json(video);
};

async function getVideoByTitle(req, res) {
    const title = req.swagger.params.name.value;
    const video = await repository.find('video', (`"name":"${title}"`));
    if (typeof video === 'undefined') {
        res.json("Video not found!")
    } else {
        res.json(video);
    }
}

async function getVideoById(req, res) {
    const videoId = await req.swagger.params.videoId.value;
    const video = await repository.findById('video', videoId);
    if (typeof video[0] === 'undefined') {
        res.json("Video not found!")
    } else {
        res.json(video);
    }
}

async function updateVideo(req, res) {
    const videoId = req.swagger.params.videoId.value;
    const updatedVideo = req.body;
    const result = await repository.updateById('video', updatedVideo, videoId);
    res.json(result);
}

async function deleteVideo(req, res) {
    const videoId = req.swagger.params.videoId.value;
    await repository.deleteById('video', videoId);
    res.json(`Video with: ${videoId} Id has been deleted!`);
}