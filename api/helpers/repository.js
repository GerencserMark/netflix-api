'use strict';

require("dotenv").config();
const rp = require('request-promise');

const DB_API_URL = process.env.DB_API_URL || 'http://localhost:3001/api/v1/';

module.exports = {
    insert,
    find,
    findById,
    deleteById,
    updateById
}

async function insert(collection, object) {
    await rp({
        method: 'POST',
        uri: DB_API_URL + collection,
        body: object,
        json: true
    });
}

async function find(collection, query) {
    const result = await rp(`${DB_API_URL}${collection}?query={${query}}`);
    return JSON.parse(result);
}

async function findById(collection, id) {
    const result = await rp({
        method: 'GET',
        uri: `${DB_API_URL}${collection}/${id}`,
        json: true
    });
    return result;
}

async function deleteById(collection, objectID) {
    const result = await rp({
        method: 'DELETE',
        uri: `${DB_API_URL}${collection}/${objectID}`,
        json: true
    });
    return result;
}

async function updateById(collection, updatedObject, ObjectId) {
    const result = await rp({
        method: 'PUT',
        uri: `${DB_API_URL}${collection}/${ObjectId}`,
        body: updatedObject,
        json: true,
    });
    return result;
}