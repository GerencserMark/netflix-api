'use strict';

const express = require('express');
const SwaggerExpress = require('swagger-express-mw');
const app = express();

module.exports = app;

const config = {
    appRoot: __dirname
};

SwaggerExpress.create(config, function (err, swaggerExpress) {
    if (err) { throw err; }

    swaggerExpress.register(app);

const port = process.env.PORT || 10010;
app.listen(port);
});